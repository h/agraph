INF := main.c
WRN := -Wall -Wextra
STD := -std=c99
OPT := -march=native -O2
LNK := -lm $(shell pkg-config --cflags --libs ncurses)
OUT := agraph

default:
	$(CC) $(STD) $(WRN) $(OPT) $(INF) $(LNK) -o $(OUT)

clean:
	rm -f ./$(out)

